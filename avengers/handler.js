"use strict";

module.exports.avengers = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*" // Required for CORS support to work
        },
        body: JSON.stringify({
          message: "This is a READ operation on Avengers "
        }) 
        
      });
      return;

      break;

    case "POST":
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*" // Required for CORS support to work
        },
        body: JSON.stringify({
          message: "This is a CREATE operation on Avengers "
        }) 
      });
      break;

    default:
      // Send HTTP 501: Not Implemented
      console.log("Error: unsupported HTTP method (" + event.httpMethod + ")");
      callback(null, { statusCode: 501 });
  }
};
