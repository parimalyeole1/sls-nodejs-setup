"use strict";

module.exports.get = (event, context, callback) => {
  
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*" // Required for CORS support to work
        },
        body: JSON.stringify({
          message: "This is a READ operation on Justice League "
        }) 
        
      });

};
